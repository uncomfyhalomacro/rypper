// SPDX-License-Identifier: MPL-2.0

use ini_core as ini;
use std::{
    error,
    fmt::{
        self,
        Display,
    },
    fs,
    path::{
        Path,
        PathBuf,
    },
};
use url::Url;

pub const ZYPP_CONFIG_PATH: &str = "/etc/zypp";
pub const ZYPP_REPO_PATH: &str = "/etc/zypp/repos.d";

#[derive(Clone, Debug, PartialEq, Hash)]
/// # Config Validity
/// Minimal valid config only requires a section AND URI.
pub struct RepoConfig
{
    /// This cannot be `None` or an empty string.
    pub baseurl: Url,
    pub optional_val: RepoConfigOptional,
}

#[derive(Clone, Debug, PartialEq, Hash)]
/// Stores the optional RepoConfig values
pub struct RepoConfigOptional
{
    pub alias: Option<String>,
    /// Value is based on `usize`. 0 is false. Greater than 1 is true.
    pub autorefresh: bool,
    /// Value is based on `usize`. 0 is false. Greater than 1 is true.
    pub enabled: bool,
    /// Value is based on `usize`. 0 is false. Greater than 1 is true.
    pub gpgcheck: bool,
    pub gpgkey: Option<Url>,
    pub name: Option<String>,
    /// Path defaults to `/`.
    pub path: Option<PathBuf>,
    /// According to `man zypper`:
    /// ```man
    /// Set the priority of the repository. Priority of 1 is the highest, and
    /// 2147483647 is the lowest. -p 0 will set the priority back
    /// to the default (99). Packages from repositories with
    /// higher priority will be used even if there are better
    /// versions available in a repository with a lower priority.
    /// ```
    /// Hence, we use u32 here. To be honest, that's overkill. 🥴
    pub priority: u32,
    /// The default is actually rpm-md but it can be just `None`. I don't really
    /// see much use of this for now. There is an alternative called RIS or [Resource Index Service](https://en.opensuse.org/openSUSE:Standards_Repository_Index_Service)
    /// This post explains its advantages: <https://news.opensuse.org/2023/07/31/try-out-cdn-with-opensuse-repos>
    pub typemd: Option<String>,
}

impl Default for RepoConfigOptional
{
    fn default() -> Self
    {
        RepoConfigOptional {
            alias: None,
            autorefresh: false,
            enabled: true,
            gpgcheck: true,
            gpgkey: None,
            name: None,
            path: None,
            priority: 99,
            typemd: None,
        }
    }
}

#[allow(dead_code)]
enum RepoOptions
{
    AutoRefresh(bool),
    Baseurl(String),
    Enabled(bool),
    Name(String),
    Path(PathBuf),
    Priority(bool),
    Section(String),
    TypeMd(String),
}

#[derive(Debug, PartialEq)]
pub enum RepoConfigErrors
{
    /// Default ZYpper will error if a section is just `[]`, thus,
    /// File contains an empty alias field
    MissingAlias
    {
        file: String
    },
    /// There is no way to request for a file or mirror if there is no URI.
    MissingUri
    {
        file: String
    },
    /// Invalid config files such as typos or missing brackets
    InvalidConfig
    {
        file: String, text: String
    },
    /// A key that requires a URI string may contain a non-URI string.
    InvalidUriString
    {
        file: String, field: String, error: url::ParseError
    },
    /// File contains no content
    EmptyConfig
    {
        file: String
    },
    FileNotFound
    {
        file: String
    },
}

impl error::Error for RepoConfigErrors {}

impl Display for RepoConfigErrors
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        match self
        {
            // TODO: Make error explicit
            RepoConfigErrors::EmptyConfig { file } =>
            {
                write!(f, "{file} contains no content")
            }
            RepoConfigErrors::MissingAlias { file } =>
            {
                write!(f, "{file} contains an empty alias field")
            }
            RepoConfigErrors::MissingUri { file } =>
            {
                write!(f, "{file} does not contain an uri")
            }
            RepoConfigErrors::InvalidConfig { file, text } =>
            {
                write!(f, "The text {text} in {file} is invalid")
            }
            RepoConfigErrors::InvalidUriString { file, field, error } =>
            {
                write!(f, "The {field} field in {file} is an invalid URI because: {error}")
            }
            RepoConfigErrors::FileNotFound { file } => write!(f, "{file} was not found"),
        }
    }
}

impl RepoConfig
{
    pub fn load_config_file(file_path: &Path) -> Result<Self, RepoConfigErrors>
    {
        validate_file_metadata(file_path)?;

        let conf = match fs::read_to_string(file_path)
        {
            Ok(file) => file,
            Err(_) =>
            {
                return Err(RepoConfigErrors::FileNotFound {
                    file: file_path.to_string_lossy().into(),
                })
            }
        };

        RepoConfig::read_config(conf.as_str(), file_path)
    }

    pub fn read_config(file_text: &str, file_path: &Path) -> Result<RepoConfig, RepoConfigErrors>
    {
        let document = ini::Parser::new(file_text.trim_start());
        // Get the default values for these parameters
        let RepoConfigOptional {
            mut alias,
            mut autorefresh,
            mut enabled,
            mut gpgcheck,
            mut gpgkey,
            mut name,
            path,
            mut priority,
            mut typemd,
        } = RepoConfigOptional::default();

        // Validate these into Url types
        let mut baseurl: Option<&str> = None;

        // We skip the first section end because
        // ```ini
        // # Some whitespace here. We stripped it though at `from` and `load_config_file`
        // # this corresponds a SectionEnd. Skipped.
        // [somesection]
        // # More stuff here
        // # This corresponds another Section End
        // ```
        for item in document.skip(1)
        {
            match item
            {
                // TODO: Should we really break this? I feel like passing it into a Vec is better?
                // Another reason why I stick to this is because repoconfig files usually have ONE
                // section only. I will improve this in the future if edge cases become more common.
                ini::Item::SectionEnd => break,
                ini::Item::Blank => continue,
                ini::Item::Comment(_) => continue,
                ini::Item::Error(err) =>
                {
                    return Err(RepoConfigErrors::InvalidConfig {
                        file: file_path.to_string_lossy().into(),
                        text: err.to_string(),
                    });
                }
                ini::Item::Section(section) =>
                {
                    if !section.is_empty()
                    {
                        alias = Some(section.to_string())
                    }
                    else
                    {
                        return Err(RepoConfigErrors::MissingAlias {
                            file: file_path.to_string_lossy().into(),
                        });
                    }
                }
                ini::Item::Property(key, some_value) => match key
                {
                    "name" => name = some_value.map(|name| name.to_string()),
                    "gpgkey" =>
                    {
                        gpgkey = match some_value
                        {
                            Some(url) => Some(validate_uri(url, "gpgkey")?),
                            None => None,
                        }
                    }
                    "baseurl" => baseurl = some_value,
                    "autorefresh" =>
                    {
                        autorefresh = match some_value
                        {
                            Some(input_string) => match input_string.parse::<usize>()
                            {
                                Ok(parsed_number) => parsed_number >= 1,
                                Err(err) => panic!("Trouble parsing string to type usize: {}", err),
                            },
                            None => false,
                        }
                    }

                    "enabled" =>
                    {
                        enabled = match some_value
                        {
                            Some(input_string) => match input_string.parse::<usize>()
                            {
                                Ok(parsed_number) => parsed_number >= 1,
                                Err(err) => panic!("Trouble parsing string to type usize: {}", err),
                            },
                            None => true,
                        }
                    }
                    "gpgcheck" =>
                    {
                        gpgcheck = match some_value
                        {
                            Some(input_string) => match input_string.parse::<usize>()
                            {
                                Ok(parsed_number) => parsed_number >= 1,
                                Err(err) => panic!("Trouble parsing string to type usize: {}", err),
                            },
                            None => true,
                        }
                    }
                    "priority" =>
                    {
                        priority = match some_value
                        {
                            Some(input_string) => match input_string.parse::<u32>()
                            {
                                Ok(parsed_number) => parsed_number,
                                Err(err) => panic!("Trouble parsing string to u32: {}", err),
                            },
                            None => 99,
                        }
                    }
                    "type" => typemd = some_value.map(|s| s.to_string()),
                    _ =>
                    {}
                },
            }
        }

        let baseurl = match baseurl
        {
            Some(url) => validate_uri(url, "baseurl"),
            None => Err(RepoConfigErrors::MissingUri { file: file_path.to_string_lossy().into() }),
        }?;

        Ok(RepoConfig {
            baseurl,
            optional_val: RepoConfigOptional {
                alias,
                autorefresh,
                enabled,
                gpgcheck,
                gpgkey,
                name,
                path,
                priority,
                typemd,
            },
        })
    }
}

/// The following layout should look like
/// ```ini
/// [alias]
/// name
/// autorefresh
/// ```
/// something like that
impl Display for RepoConfig
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        let baseurl = self.baseurl.clone();
        let RepoConfigOptional {
            alias,
            autorefresh,
            enabled,
            gpgcheck,
            gpgkey,
            name,
            priority,
            typemd,
            ..
        } = self.optional_val.clone();
        let mut output = String::new();

        match alias
        {
            Some(alias) => output.push_str(format!("[{alias}]\n").as_str()),
            None => output.push_str("[]\n"),
        };

        if let Some(name) = name
        {
            output.push_str(format!("name={name}\n").as_str());
        }

        if enabled
        {
            output.push_str("enabled=1\n");
        }
        else
        {
            output.push_str("enabled=0\n");
        }

        if autorefresh
        {
            output.push_str("autorefresh=1\n");
        }
        else
        {
            output.push_str("autorefresh=0\n");
        }

        output.push_str(format!("baseurl={baseurl}\n").as_str());

        if let Some(typemd) = typemd
        {
            output.push_str(format!("type={typemd}\n").as_str());
        }

        if !gpgcheck
        {
            output.push_str("gpgcheck=0\n");
        }

        if let Some(gpgkey) = gpgkey
        {
            output.push_str(format!("gpgkey={gpgkey}\n").as_str());
        }

        let default_priority = RepoConfigOptional::default().priority;

        if priority != default_priority
        {
            output.push_str(format!("priority={priority}\n").as_str());
        }

        write!(f, "{}", output.as_str())
    }
}

/// Checking if a URI is valid is essential for
/// reading `baseurl` and `gpgkey` parameters.
///
/// # A valid URI should be like this:
/// ```
/// use rypper_reader::repoconfig;
/// use url::Url;
///
/// let uri = "https://example.com";
/// match repoconfig::validate_uri(uri, "dummy") {
///   Ok(c) => {
///     assert_eq!(c, Url::parse("https://example.com").unwrap());
///   }
///   Err(_) => {}
/// };
/// ```
pub fn validate_uri(uristring: &str, field: &str) -> Result<Url, RepoConfigErrors>
{
    match uristring
    {
        "" => Err(RepoConfigErrors::MissingUri { file: uristring.to_string() }),
        url => Url::parse(url).map_err(|error| RepoConfigErrors::InvalidUriString {
            file: uristring.to_string(),
            field: field.to_string(),
            error,
        }),
    }
}

pub fn validate_file_metadata(file_path: &Path) -> Result<(), RepoConfigErrors>
{
    let file_md = match std::fs::metadata(file_path)
    {
        Ok(c) => c,
        Err(_) =>
        {
            return Err(RepoConfigErrors::FileNotFound {
                file: file_path.to_string_lossy().to_string(),
            });
        }
    };

    if file_md.len() == 0
    {
        return Err(RepoConfigErrors::EmptyConfig { file: file_path.to_string_lossy().into() });
    }
    Ok(())
}

pub fn load_config_files(dir_path: &Path) -> Result<Vec<RepoConfig>, String>
{
    let mut repos: Vec<RepoConfig> = Vec::new();

    let dir = std::fs::read_dir(dir_path).map_err(|e| e.to_string())?;

    for conf_path in dir
    {
        match RepoConfig::load_config_file(&conf_path.map_err(|e| e.to_string())?.path())
        {
            Ok(repo) => repos.push(repo),
            Err(e) => Err(e.to_string())?,
        }
    }

    Ok(repos)
}

// TODO: Move tests to separate file or folder
#[cfg(test)]
mod tests
{

    use super::*;
    use std::env;

    // Test equality from config file with another config file and/or variable
    // declared config + default.
    #[test]
    fn equal_repoconfig() -> Result<(), RepoConfigErrors>
    {
        let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let file_path = format!("{}/samples/home_uncomfyhalomacro.repo", manifest_dir);
        let another_file_path =
            format!("{}/samples/another_home_uncomfyhalomacro.repo", manifest_dir);
        let example_config = RepoConfig::load_config_file(Path::new(file_path.as_str()))?;
        // This one contains a file which should have same key-values but of different
        // arrangements.
        let another_example_config =
            RepoConfig::load_config_file(Path::new(another_file_path.as_str()))?;
        let config = RepoConfig {
            baseurl:  Url::parse("https://download.opensuse.org/repositories/home:/uncomfyhalomacro/openSUSE_Tumbleweed/").unwrap(),
            optional_val: RepoConfigOptional {
                alias:  Some(String::from("home_uncomfyhalomacro")),
                autorefresh:  true,
                enabled:  true,
                gpgcheck:  true,
                gpgkey:  Some(Url::parse("https://download.opensuse.org/repositories/home:/uncomfyhalomacro/openSUSE_Tumbleweed/repodata/repomd.xml.key").unwrap()),
                priority: 99,
                typemd:  None,
                .. Default::default()
            },
        };
        assert_eq!(example_config, config);
        assert_eq!(another_example_config, example_config);
        Ok(())
    }

    #[test]
    fn invalid_baseurl()
    {
        let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let file_path = format!("{}/samples/invalid_baseurl.repo", manifest_dir);
        assert_eq!(true, RepoConfig::load_config_file(Path::new(file_path.as_str())).is_err());
    }

    #[test]
    fn invalid_gpgkey_uri()
    {
        let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let file_path = format!("{}/samples/invalid_gpgkey_uri.repo", manifest_dir);
        assert_eq!(true, RepoConfig::load_config_file(Path::new(file_path.as_str())).is_err());
    }

    #[test]
    fn errors_without_baseurl()
    {
        let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let file_path = format!("{}/samples/no_baseurl.repo", manifest_dir);
        assert_eq!(true, RepoConfig::load_config_file(Path::new(file_path.as_str())).is_err());
    }

    #[test]
    fn works_with_only_section_and_baseurl() -> Result<(), RepoConfigErrors>
    {
        let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let file_path = format!("{}/samples/only_section_and_baseurl.repo", manifest_dir);
        let config = RepoConfig::load_config_file(Path::new(file_path.as_str()))?;
        assert_eq!(Some("a_section".to_string()), config.optional_val.alias);
        assert_eq!(Url::parse("https://example.com").unwrap(), config.baseurl);
        Ok(())
    }

    #[test]
    fn works_with_only_section_and_baseurl_from_str() -> Result<(), RepoConfigErrors>
    {
        let document = r#"[section]
baseurl=https://example.com
"#;
        let config = RepoConfig::read_config(
            &document,
            Path::new("works_with_only_section_and_baseurl_from_str test"),
        )?;
        println!("{:#?}", config);
        assert_eq!(Some("section".to_string()), config.optional_val.alias);
        assert_eq!(Url::parse("https://example.com").unwrap(), config.baseurl);
        Ok(())
    }

    #[test]
    fn works_with_too_many_whitespaces_for_valid_config() -> Result<(), RepoConfigErrors>
    {
        let document = r#"





[section]
baseurl=https://example.com
"#;
        let config = RepoConfig::read_config(
            &document,
            Path::new("works_with_too_many_whitespaces_for_valid_config test"),
        )?;
        println!("{:#?}", config);
        assert_eq!(Some("section".to_string()), config.optional_val.alias);
        assert_eq!(Url::parse("https://example.com").unwrap(), config.baseurl);
        Ok(())
    }

    #[test]
    fn invalid_if_with_non_existent_file()
    {
        let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let file_path = format!("{}/samples/dummy.repo", manifest_dir);
        assert_eq!(true, RepoConfig::load_config_file(Path::new(file_path.as_str())).is_err());
        if let Err(err) = RepoConfig::load_config_file(Path::new(file_path.as_str()))
        {
            assert_eq!(RepoConfigErrors::FileNotFound { file: file_path }, err)
        }
    }

    #[test]
    fn invalid_if_no_section()
    {
        let document = "baseurl=https://example.com";
        let config = RepoConfig::read_config(&document, Path::new("invalid_if_no_section test"));
        assert_eq!(true, config.is_err());
    }

    #[test]
    fn invalid_if_empty_str()
    {
        let document = "";
        let config = RepoConfig::read_config(&document, Path::new("invalid_if_empty_str test"));
        assert_eq!(true, config.is_err());
    }

    #[test]
    fn invalid_if_empty_config()
    {
        let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let file_path = format!("{}/samples/empty_config.repo", manifest_dir);
        assert_eq!(true, RepoConfig::load_config_file(Path::new(file_path.as_str())).is_err());
    }
}
