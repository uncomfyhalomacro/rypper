use rypper_core::run_rypper;

fn main()
{
    if let Err(e) = run_rypper()
    {
        println!("Error: {e}");
    }
}
