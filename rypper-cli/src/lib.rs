use clap::{
    Parser,
    Subcommand,
};

#[derive(Parser, Debug)]
#[command(name = "rypper", version)]
pub struct Cli
{
    /// Suppress normal output, print only error messages
    #[arg(long, short = 'q')]
    pub quiet: bool,

    /// Do not ask anything, use default answers automatically
    #[arg(long = "non-interactive", short = 'n')]
    pub non_interactive: bool,

    #[command(subcommand)]
    pub command: Command,
}

#[derive(Subcommand, Debug)]
pub enum Command
{
    /// Install packages
    #[clap(visible_alias("in"))]
    Install,

    /// Remove packages
    #[clap(visible_alias("rm"))]
    Remove,

    /// Update installed packages with newer versions
    #[clap(visible_alias("up"))]
    Update,

    /// Perform a distribution upgrade
    #[clap(visible_alias("dup"))]
    DistUpgrade,

    /// List available updates
    #[clap(visible_alias("lu"))]
    ListUpdates,

    /// Install needed patches
    Patch,

    /// List available patches
    #[clap(visible_alias("lp"))]
    ListPatches,

    /// List all repositories
    #[clap(visible_alias("lr"))]
    Repos
    {
        /// Export all defined repositories as a single local .repo file
        #[arg(long, short = 'e')]
        export: Option<String>,

        /// Show also base URI of repositories
        #[arg(long, short = 'u')]
        uri: bool,

        /// Show also repository priority
        #[arg(long, short = 'p')]
        priority: bool,

        /// Show more information like URI, priority, type
        #[arg(long, short = 'd')]
        details: bool,

        /// Show enabled repos only
        #[arg(long, short = 'E')]
        show_enabled_only: bool,
    },

    /// Add a new repository
    #[clap(visible_alias("ar"))]
    AddRepo,

    /// Remove a repository
    #[clap(visible_alias("rr"))]
    RemoveRepo,

    /// Modify a repository
    #[clap(visible_alias("mr"))]
    ModifyRepo,

    /// List package locks
    #[clap(visible_alias("ll"))]
    Locks,

    /// Add a package lock
    #[clap(visible_alias("al"))]
    AddLock,

    /// Remove a package lock
    #[clap(visible_alias("rl"))]
    RemoveLock,

    /// Search for packages matching a pattern
    #[clap(visible_alias("se"))]
    Search,

    /// Show information for specified packages
    #[clap(visible_alias("if"))]
    Info,
}

#[cfg(test)]
mod tests
{
    #[test]
    fn it_works()
    {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
