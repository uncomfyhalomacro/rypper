use clap::Parser as _;
use rypper_cli::{
    Cli,
    Command,
};
use rypper_reader::repoconfig;
use std::{
    fs::File,
    io::Write,
    path::PathBuf,
};
use tabled::{
    builder::Builder as TableBuilder,
    settings::Style as TableStyle,
};

/// Does as it says on the tin, exists to pass on errors to main() which can
/// then be emitted
pub fn run_rypper() -> Result<(), String>
{
    let Cli { quiet, non_interactive, command } = Cli::parse();

    match command
    {
        Command::Repos { export, uri, priority, details, show_enabled_only } =>
        {
            let repo_dir: PathBuf = "/etc/zypp/repos.d".into();
            let configs = repoconfig::load_config_files(&repo_dir)?;

            // Determine if we are exporting a repo file or just displaying a table of
            // information
            match export
            {
                Some(file_name) =>
                {
                    let mut file_text = String::new();
                    for repo in configs
                    {
                        file_text += repo.to_string().as_str();
                    }
                    let mut file =
                        File::create(format!("{file_name}.repo")).map_err(|e| e.to_string())?;
                    file.write_all(file_text.as_bytes()).map_err(|e| e.to_string())?;
                }
                None =>
                {
                    let mut table_build = TableBuilder::default();
                    let mut table_header =
                        vec!["#", "Alias", "Name", "Enabled", "GPG Check", "Refresh"];

                    if details || priority
                    {
                        table_header.push("Priority");
                    }

                    if details
                    {
                        table_header.push("Type");
                    }

                    if details || uri
                    {
                        table_header.push("URI");
                    }

                    table_build.push_record(table_header);

                    for (i, config) in configs.iter().enumerate()
                    {
                        // Skip an entry if it is not enabled
                        let enabled = config.optional_val.enabled;
                        if show_enabled_only && !enabled
                        {
                            continue;
                        }

                        let alias = config.optional_val.alias.clone().unwrap_or_default();
                        let name = config.optional_val.name.clone().unwrap_or_default();
                        let enabled = yes_or_no(enabled);
                        let gpg_check = yes_or_no(config.optional_val.gpgcheck);
                        let autorefresh = yes_or_no(config.optional_val.autorefresh);

                        let mut record: Vec<String> =
                            vec![i.to_string(), alias, name, enabled, gpg_check, autorefresh];

                        if details || priority
                        {
                            let priority = config.optional_val.priority.to_string();
                            record.push(priority);
                        }

                        if details
                        {
                            let typemd = match config.optional_val.typemd.clone()
                            {
                                Some(t) => t,
                                None => "N/A".to_string(),
                            };
                            record.push(typemd);
                        }

                        if details || uri
                        {
                            let uri = config.baseurl.to_string();
                            record.push(uri);
                        }
                        table_build.push_record(record);
                    }

                    let table = table_build.build().with(TableStyle::markdown()).to_string();
                    println!("{table}");
                }
            }
            Ok(())
        }
        cmd => Err(format!("{cmd:?} has not been implemented yet")),
    }
}

fn yes_or_no(b: bool) -> String
{
    if b
    {
        "Yes".to_string()
    }
    else
    {
        "No".to_string()
    }
}
